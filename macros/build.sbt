name := """macros"""

version := "1.0"

scalaVersion := "2.11.3"

resolvers ++= Seq("Typesafe Releases" at "http://repo.typesafe.com/typesafe/releases/")

libraryDependencies ++= Seq("com.typesafe.play" %% "anorm" % "2.3.5")

// Change this to another test framework if you prefer
libraryDependencies += "org.scalatest" %% "scalatest" % "2.1.6" % "test"

libraryDependencies += "org.scala-lang" % "scala-reflect" % "2.11.1"

// Uncomment to use Akka
//libraryDependencies += "com.typesafe.akka" % "akka-actor_2.11" % "2.3.3"

