import play.PlayScala

name := """macros-play"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file("."))
  .enablePlugins(PlayScala)
  .aggregate(macros)
  .dependsOn(macros)

lazy val macros = project

scalaVersion := "2.11.3"

libraryDependencies ++= Seq(
  jdbc,
  anorm,
  cache,
  ws
)



